package org.tduk.whispir.action;

import org.tduk.whispir.Bot;
import org.tduk.whispir.BotException;
import org.tduk.whispir.DirectionEnum;
import org.tduk.whispir.Table;

public class ActionRight extends ActionBase {
	public static String ACTION_NAME = "RIGHT";

	public ActionRight(Table table) {
		super(table);
	}

	@Override
	public String getName() {
		return ACTION_NAME;
	}

	@Override
	public void perform(Bot bot, String... args) throws BotException {
		if (!bot.isPlaced()) {
			throw new BotException("Bot need to be placed on a table first");
		}

		switch (bot.getDirection()) {
		case WEST:
			bot.setDirection(DirectionEnum.NORTH);
			break;
		case NORTH:
			bot.setDirection(DirectionEnum.EAST);
			break;
		case EAST:
			bot.setDirection(DirectionEnum.SOUTH);
			break;
		case SOUTH:
			bot.setDirection(DirectionEnum.WEST);
		}
	}

}
