package org.tduk.whispir.action;

import org.tduk.whispir.Bot;
import org.tduk.whispir.BotException;
import org.tduk.whispir.Table;

public class ActionMove extends ActionBase{
	public static String ACTION_NAME="MOVE";
	
	public ActionMove(Table table) {
		super(table);
	}

	@Override
	public String getName() {
		return ACTION_NAME;
	}

	@Override
	public void perform(Bot bot, String... args) throws BotException {
		if(!bot.isPlaced()){
			throw new BotException("Bot need to be placed on a table first");
		}
		
		int x = bot.getX();
		int y = bot.getY();
		
		switch (bot.getDirection()) {
		case EAST: 
			x++;
			break;
		case WEST:
			x--;
			break;
		case NORTH:
			y++;
			break;
		case SOUTH:
			y--;
		}
		
		if( x < 0 || x >= table.getWidth() || y < 0 || y >= table.getHeight()){
			throw new BotException("Bot is on the edge, cannot complete a move");
		}
		
		bot.setX(x);
		bot.setY(y);
	}

}
