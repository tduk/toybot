package org.tduk.whispir.action;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.junit.Assert.*;
import org.junit.Test;
import org.tduk.whispir.BotException;
import org.tduk.whispir.DirectionEnum;
import org.tduk.whispir.Table;

public class ActionMoveTest extends ActionBaseTest {

	private void test_cannot_move(DirectionEnum initDirection) {
		when(bot.getDirection()).thenReturn(initDirection);

		when(table.getHeight()).thenReturn(1);
		when(table.getWidth()).thenReturn(1);

		when(bot.getX()).thenReturn(0);
		when(bot.getY()).thenReturn(0);

		try {
			action.perform(bot);
			fail("Expected BotExcetpion is not thrown");
		} catch (BotException e) {
			assertEquals("Bot is on the edge, cannot complete a move", e.getMessage());
		}

	}

	private void test_perform_move(DirectionEnum initDirection, int destX, int destY) throws BotException {
		when(table.getHeight()).thenReturn(3);
		when(table.getWidth()).thenReturn(3);

		when(bot.getX()).thenReturn(1);
		when(bot.getY()).thenReturn(1);

		when(bot.getDirection()).thenReturn(initDirection);
		
		action.perform(bot);
		
		verify(bot, times(1)).setX(destX);
		verify(bot, times(1)).setY(destY);
	}

	@Override
	protected ActionBase initAction(Table table) {
		return new ActionMove(table);
	}

	@Test
	public void test_perform_canot_move_east() throws BotException {
		test_cannot_move(DirectionEnum.EAST);
	}

	@Test
	public void test_perform_cannot_move_west() throws BotException {
		test_cannot_move(DirectionEnum.WEST);
	}

	@Test
	public void test_perform_cannot_move_north() throws BotException {
		test_cannot_move(DirectionEnum.NORTH);
	}

	@Test
	public void test_perform_cannot_move_south() throws BotException {
		test_cannot_move(DirectionEnum.SOUTH);
	}

	@Test
	public void test_perform_move_east() throws BotException {
		test_perform_move(DirectionEnum.EAST, 2, 1);
	}

	@Test
	public void test_perform_move_west() throws BotException {
		test_perform_move(DirectionEnum.WEST, 0, 1);
	}

	@Test
	public void test_perform_move_north() throws BotException {
		test_perform_move(DirectionEnum.NORTH, 1, 2);
	}

	@Test
	public void test_perform_move_south() throws BotException {
		test_perform_move(DirectionEnum.SOUTH, 1, 0);
	}
}
