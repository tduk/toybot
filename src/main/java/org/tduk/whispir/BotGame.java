package org.tduk.whispir;

import java.util.Arrays;
import java.util.Scanner;

import org.tduk.whispir.action.ActionBase;
import org.tduk.whispir.action.ActionFactory;

public class BotGame {
	public static final int WIDTH = 5;
	public static final int HEIGHT = 5;

	protected Table table;
	protected Bot bot;
	protected ActionFactory actionFactory;

	Scanner in = new Scanner(System.in);

	
	protected void init() {
		table = new Table(WIDTH, HEIGHT);
		actionFactory = new ActionFactory(table);
		bot = new Bot();
	}

	protected void executeCommand(String commandLine) throws BotException {
		if (commandLine != null && commandLine.length() != 0) {
			String actionTokens[] = commandLine.split("[,\\s]+");

			ActionBase action = actionFactory.getAction(actionTokens[0].trim());

			if (action == null) {
				throw new BotException("Action doesn't exists");
			}

			String[] args = actionTokens.length > 1 ? Arrays.copyOfRange(actionTokens, 1, actionTokens.length)
					: new String[0];

			action.perform(bot, args);
		} else {
			throw new BotException("Invalid action");
		}
	}

	
	public String readCommand(){
		System.out.println("Please provide next action for a bot");
		System.out.println("Available actions are " + actionFactory.getAvailableActions());

		return in.nextLine();
	}
	/*
	 * Main loop
	 */
	public void run() {
		while (true) {
			String commandLine = readCommand();
			
			if (commandLine.length() == 0) {
				break;
			}

			try {
				executeCommand(commandLine);
			} catch (BotException e) {
				System.out.println("Error: " + e.getMessage());
			}
		}

		in.close();

		System.out.println("Exit...");
	}

	public static void main(String[] args) {
		BotGame botGame = new BotGame();
		botGame.init();
		botGame.run();
	}
}
