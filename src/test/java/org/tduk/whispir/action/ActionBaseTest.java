package org.tduk.whispir.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.tduk.whispir.Bot;
import org.tduk.whispir.BotException;
import org.tduk.whispir.DirectionEnum;
import org.tduk.whispir.Table;

public abstract class ActionBaseTest {
	protected ActionBase action;
	protected Table table;
	protected Bot bot;

	protected abstract ActionBase initAction(Table table);

	@Before
	public void before() {
		table = mock(Table.class);
		when(table.getHeight()).thenReturn(1);
		when(table.getWidth()).thenReturn(1);

		bot = mock(Bot.class);
		when(bot.getX()).thenReturn(0);
		when(bot.getY()).thenReturn(0);
		when(bot.isPlaced()).thenReturn(true);
		when(bot.getDirection()).thenReturn(DirectionEnum.EAST);

		action = initAction(table);
	}

	@Test
	public void test_init() {
		assertEquals(table, action.table);
	}

	@Test
	public void test_perform_not_placed() {
		when(bot.isPlaced()).thenReturn(false);
		try {
			action.perform(bot);
			fail("Expected BotException is not thrown");
		} catch (BotException e) {
			assertEquals("Bot need to be placed on a table first", e.getMessage());
		}
	}

}
