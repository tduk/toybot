package org.tduk.whispir.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.tduk.whispir.Table;

public class ActionFactory {
	private Map<String, ActionBase> actions;

	public ActionFactory(Table table) {
		actions = new HashMap<String, ActionBase>();

		actions.put(ActionPlace.ACTION_NAME, new ActionPlace(table));
		actions.put(ActionReport.ACTION_NAME, new ActionReport(table));
		actions.put(ActionMove.ACTION_NAME, new ActionMove(table));
		actions.put(ActionRight.ACTION_NAME, new ActionRight(table));
		actions.put(ActionLeft.ACTION_NAME, new ActionLeft(table));
	}

	public ActionBase getAction(String actionName) {
		return actions.get(actionName);
	}

	public Set<String> getAvailableActions() {
		return actions.keySet();
	}

}
