package org.tduk.whispir.action;

import org.junit.Test;
import static  org.mockito.Mockito.*;
import org.tduk.whispir.BotException;
import org.tduk.whispir.Table;

public class ActionReportTest extends ActionBaseTest{
	@Test
	public void test_perform_success() throws BotException {
		action.perform(bot);
		
		verify(bot, times(1)).getX();
		verify(bot, times(1)).getY();
		verify(bot, times(1)).getDirection();

	}

	@Override
	protected ActionBase initAction(Table table) {
		return new ActionReport(table);
	}

}
