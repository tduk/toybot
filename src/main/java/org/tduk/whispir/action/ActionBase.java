package org.tduk.whispir.action;

import org.tduk.whispir.Bot;
import org.tduk.whispir.BotException;
import org.tduk.whispir.Table;
/**
 * Base action class for all Bot's actions.
 * 
 * @author taras
 *
 */
public abstract class ActionBase {
	protected Table table;
	
	public ActionBase(Table table){
		this.table = table;
	}
	public abstract String getName();
	
	public abstract void perform(Bot bot, String... args) throws BotException;
}
