package org.tduk.whispir.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.tduk.whispir.Table;

public class ActionFactoryTest {
	private static final String AVAILABLE_ACTIONS = "LEFT, MOVE, RIGHT, REPORT, PLACE";
	private Set<String> availableActions;
	private ActionFactory actionFactory;
	private Table table;

	@Before
	public void beforeClass() {
		availableActions = new HashSet<String>();

		availableActions.addAll(Arrays.asList(AVAILABLE_ACTIONS));

		table = new Table(0, 0);

		actionFactory = new ActionFactory(table);
	}

	@Test
	public void test_init() {
		for (String actionName : actionFactory.getAvailableActions()) {
			ActionBase action = actionFactory.getAction(actionName);
			assertEquals(table, action.table);
		}
	}

	@Test
	public void test_get_available_actions() {
		assertEquals(availableActions.toString(), actionFactory.getAvailableActions().toString());
	}

	@Test
	public void test_get_action() {
		String[] availableActions = AVAILABLE_ACTIONS.split(", ");
		for (String actionName : availableActions) {
			assertEquals(actionName, actionFactory.getAction(actionName).getName());
		}
	}

	@Test
	public void test_get_action_null() {
		assertNull(actionFactory.getAction("NONE"));
	}
}
