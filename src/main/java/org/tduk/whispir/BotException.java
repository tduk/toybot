package org.tduk.whispir;

public class BotException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3456398540411094145L;

	public BotException(String msg){
		super(msg);
	}
}
