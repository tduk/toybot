package org.tduk.whispir.action;

import org.tduk.whispir.Bot;
import org.tduk.whispir.BotException;
import org.tduk.whispir.Table;

public class ActionReport extends ActionBase {
	public static String ACTION_NAME = "REPORT";

	public ActionReport(Table table) {
		super(table);
	}

	@Override
	public String getName() {
		return ACTION_NAME;
	}

	@Override
	public void perform(Bot bot, String... args) throws BotException {
		if (!bot.isPlaced()) {
			throw new BotException("Bot need to be placed on a table first");
		}

		System.out.println(bot.getX() + "," + bot.getY() + "," + bot.getDirection());
	}

}
