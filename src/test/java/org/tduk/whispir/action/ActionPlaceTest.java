package org.tduk.whispir.action;

import static org.junit.Assert.*;
import org.junit.Test;
import org.tduk.whispir.BotException;
import org.tduk.whispir.Table;

public class ActionPlaceTest extends ActionBaseTest {
	private void test_perform_invalid_arg(String expectedMsg, String... args) {
		try {
			action.perform(bot, args);
			fail("Exptected BotException is not thrown");
		} catch (BotException e) {
			assertEquals(expectedMsg, e.getMessage());
		}
	}

	private void test_perform_place_out_range(String x, String y) {
		try {
			action.perform(bot, x, y, "NORTH");
			fail("Expected BotExcetpion is not thrown");
		} catch (BotException e) {
			assertEquals("Cannot place a bot there - out of range", e.getMessage());
		}

	}

	@Test
	public void test_perform_not_placed() {
		// super.test_perform_not_placed is not required for ActionPlace
	}

	@Test
	public void test_perform_invalid_args() throws BotException {
		test_perform_invalid_arg("Invalid number of arguments. Usage: PLACE <X>, <Y>, <Direction>", "test", "test");
	}

	@Test
	public void test_perform_invalid_x() throws BotException {
		test_perform_invalid_arg("Argument 'X' is invalid.", "s", "1", "NORTH");
	}

	@Test
	public void test_perform_invalid_y() throws BotException {
		test_perform_invalid_arg("Argument 'Y' is invalid.", "1", "a", "NORTH");
	}

	@Test
	public void test_perform_invalid_direction() throws BotException {
		test_perform_invalid_arg("Argument 'Direction' is invalid", "1", "1", "test");
	}

	@Test
	public void test_perform_x_too_big() throws BotException {
		test_perform_place_out_range("1", "0");
	}

	@Test
	public void test_perform_x_too_small() throws BotException {
		test_perform_place_out_range("-1", "0");
	}

	@Test
	public void test_perform_y_too_big() throws BotException {
		test_perform_place_out_range("0", "1");
	}

	@Test
	public void test_perform_y_too_small() throws BotException {
		test_perform_place_out_range("0", "-1");
	}

	@Override
	protected ActionBase initAction(Table table) {
		return new ActionPlace(table);
	}

}
