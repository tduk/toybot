package org.tduk.whispir.action;

import org.tduk.whispir.Bot;
import org.tduk.whispir.BotException;
import org.tduk.whispir.DirectionEnum;
import org.tduk.whispir.Table;

public class ActionPlace extends ActionBase {
	public static final String ACTION_NAME = "PLACE";

	public ActionPlace(Table table) {
		super(table);
	}

	public void perform(Bot bot, String... args) throws BotException {
		if (args == null || args.length != 3) {
			throw new BotException("Invalid number of arguments. Usage: PLACE <X>, <Y>, <Direction>");
		}
		int x, y;
		try {
			x = Integer.parseInt(args[0]);
		} catch (Exception e) {
			throw new BotException("Argument 'X' is invalid.");
		}

		try {
			y = Integer.parseInt(args[1]);
		} catch (Exception e) {
			throw new BotException("Argument 'Y' is invalid.");
		}

		DirectionEnum direction;
		
		try {
			direction = DirectionEnum.valueOf(args[2]);
		} catch (Exception ex) {
			throw new BotException("Argument 'Direction' is invalid");
		}

		if (x >= 0 && x < table.getWidth() && y >= 0 && y < table.getHeight()) {
			bot.setX(x);
			bot.setY(y);
			bot.setDirection(direction);
			bot.setPlaced(true);
		} else {
			throw new BotException("Cannot place a bot there - out of range");
		}
	}

	@Override
	public String getName() {
		return ACTION_NAME;
	}

}
