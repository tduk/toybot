package org.tduk.whispir;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.tduk.whispir.action.ActionBase;
import org.tduk.whispir.action.ActionFactory;

public class BotGameTest {
	public static String ACTION_NAME="ACTION";
	
	BotGame botGame = new BotGame();
	ActionBase action;
	
	@Before
	public void before() {
		botGame = new BotGame();
		botGame.actionFactory = mock(ActionFactory.class);
		botGame.bot = mock(Bot.class);
		botGame.table = mock(Table.class);
		
		action = mock(ActionBase.class);
		when(action.getName()).thenReturn(ACTION_NAME);
	
		Set<String> actions = new HashSet<String>();
		actions.add(ACTION_NAME);
		
		when(botGame.actionFactory.getAvailableActions()).thenReturn(actions);
		when(botGame.actionFactory.getAction(ACTION_NAME)).thenReturn(action);
	}

	@Test
	public void test_init() {
		BotGame botGame = new BotGame();
		botGame.init();

		assertNotNull(botGame.bot);
		assertNotNull(botGame.actionFactory);
		
		assertNotNull(botGame.table);
		assertEquals(botGame.table.getWidth(), BotGame.WIDTH);
		assertEquals(botGame.table.getHeight(), BotGame.HEIGHT);
		
		
	}

	private void test_execute_command_fail(String command, String errorMsg) {
		try {
			botGame.executeCommand(command);
			fail("Expected BotException is not thrown");
		} catch (BotException e) {
			assertEquals(errorMsg, e.getMessage());
		}

	}

	@Test
	public void test_execute_command_empty_command() throws BotException {
		test_execute_command_fail("", "Invalid action");
	}

	@Test
	public void test_execute_command_null_command() throws BotException {
		test_execute_command_fail(null, "Invalid action");
	}

	@Test
	public void test_execute_command_not_exists() {
		test_execute_command_fail("test", "Action doesn't exists");
	}
	
	@Test
	public void test_execute_command_no_args() throws BotException{
		botGame.executeCommand(ACTION_NAME);
		
		verify(botGame.actionFactory,times(1)).getAction(ACTION_NAME);
		verify(action, times(1)).perform(botGame.bot);
	}
	
	@Test
	public void test_execute_command_multiple_args() throws BotException{
		botGame.executeCommand(ACTION_NAME+" 1,1,NORTH");
		
		verify(botGame.actionFactory,times(1)).getAction(ACTION_NAME);
		verify(action, times(1)).perform(botGame.bot,"1","1","NORTH");
	}
	
	@Test
	public void test_execute_command_trailing_spaces() throws BotException{
		botGame.executeCommand(ACTION_NAME+" 1, 1, NORTH");
		
		verify(botGame.actionFactory,times(1)).getAction(ACTION_NAME);
		verify(action, times(1)).perform(botGame.bot,"1","1","NORTH");
	}

	
}
