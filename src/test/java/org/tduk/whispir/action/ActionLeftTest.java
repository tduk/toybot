package org.tduk.whispir.action;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.tduk.whispir.BotException;
import org.tduk.whispir.DirectionEnum;
import org.tduk.whispir.Table;

public class ActionLeftTest extends ActionBaseTest {
	private void testTurn(DirectionEnum initDirection, DirectionEnum expectedDirection) throws BotException {
		when(bot.isPlaced()).thenReturn(true);

		when(bot.getDirection()).thenReturn(initDirection);

		action.perform(bot);

		verify(bot, times(1)).setDirection(expectedDirection);
	}

	@Override
	protected ActionBase initAction(Table table) {
		return new ActionLeft(table);
	}

	@Test
	public void test_perform_south() throws BotException {
		testTurn(DirectionEnum.WEST, DirectionEnum.SOUTH);
	}

	@Test
	public void test_perform_east() throws BotException {
		testTurn(DirectionEnum.SOUTH, DirectionEnum.EAST);
	}

	@Test
	public void test_perform_north() throws BotException {
		testTurn(DirectionEnum.EAST, DirectionEnum.NORTH);
	}

	@Test
	public void test_perform_west() throws BotException {
		testTurn(DirectionEnum.NORTH, DirectionEnum.WEST);
	}

}
